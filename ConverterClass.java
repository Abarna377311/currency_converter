
public class ConverterClass extends USDClass {
String destCurrency;
float conv_amt = 0;
	ConverterClass(float usdQty,String destCurrency)
	{
		super(usdQty);
		this.destCurrency=destCurrency;
	}
	float convertCurrency()
	{
		if(destCurrency.equals("EUR"))
		{
			conv_amt=(float) (usdQty*0.81);
		}
		if(destCurrency.equals("INR"))
		{
			conv_amt=(float) (usdQty*64.31);
		}
		if(destCurrency.equals("CAD"))
		{
			conv_amt=(float) (usdQty*1.26);
		}
		if(destCurrency.equals("GBP"))
		{
			conv_amt=(float) (usdQty*0.72);
		}
		if(destCurrency.equals("SGD"))
		{
			conv_amt=(float) (usdQty*1.32);
		}
		if(destCurrency.equals("MYR"))
		{
			conv_amt=(float) (usdQty*3.95);
		}
		return conv_amt;
	}
	void display()
	{
	System.out.println("The "+destCurrency+" equivalent to "+usdQty+" USD is :"+convertCurrency());	
	}
}
